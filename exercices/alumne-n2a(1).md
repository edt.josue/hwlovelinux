# Extract hardware info from host with linux

# system


#### system model, bios version and bios date, how old is the hardware?

to see the system model, we must use the dmidecode -s sytem-manufaturer command on root user
```
# dmidecode -s system-manufacturer
Gigabyte Technology Co., Ltd.
```
to see the bios version and bios date, we must use the dmidecode -s bios-version command and dmidecode -s bios-release-date on root user
```
[root@a03 ~]# dmidecode -s bios-release-date
01/17/2014
[root@a03 ~]# dmidecode -s bios-version
F5
```

## mainboard model, link to manual, link to product


### memory banks (free or occupied)
```
[root@a03 ~]# lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]
```
### how many disks and types can be connected

### chipset, link to 

[page 5](https://download.gigabyte.com/FileList/Manual/mb_manual_ga-h81-d3_e.pdf)

## cpu

### cpu model, year, cores, threads, cache 
```
[root@a03 ~]# lscpu
Architecture:                    x86_64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
Address sizes:                   39 bits physical, 48 bits virtual
CPU(s):                          2
On-line CPU(s) list:             0,1
Thread(s) per core:              1
Core(s) per socket:              2
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       GenuineIntel
CPU family:                      6
Model:                           60
Model name:                      Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
Stepping:                        3
CPU MHz:                         1014.737
CPU max MHz:                     3200.0000
CPU min MHz:                     800.0000
BogoMIPS:                        6385.00
Virtualization:                  VT-x
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        3 MiB
NUMA node0 CPU(s):               0,1

```
### socket 
```

```
## pci

### number of pci slots, lanes available
```
# lspci
00:00.0 Host bridge: Intel Corporation 4th Gen Core Processor DRAM Controller (rev 06)
00:02.0 VGA compatible controller: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller (rev 06)
00:03.0 Audio device: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller (rev 06)
00:14.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB xHCI (rev 05)
00:16.0 Communication controller: Intel Corporation 8 Series/C220 Series Chipset Family MEI Controller #1 (rev 04)
00:1a.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #2 (rev 05)
00:1b.0 Audio device: Intel Corporation 8 Series/C220 Series Chipset High Definition Audio Controller (rev 05)
00:1c.0 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #1 (rev d5)
00:1c.2 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #3 (rev d5)
00:1c.3 PCI bridge: Intel Corporation 82801 PCI Bridge (rev d5)
00:1d.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #1 (rev 05)
00:1f.0 ISA bridge: Intel Corporation H81 Express LPC Controller (rev 05)
00:1f.2 SATA controller: Intel Corporation 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode] (rev 05)
00:1f.3 SMBus: Intel Corporation 8 Series/C220 Series Chipset Family SMBus Controller (rev 05)
02:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 06)
03:00.0 PCI bridge: Intel Corporation 82801 PCI Bridge (rev 41)
```
### devices connected

### network device, model, kernel module, speed

### audio device, model, kernel module

### vga device, model, kernel module

## hard disks

### /dev/* , model, bus type, bus speed

### 3 test fio random (IOPS) and sequential (MBps)
