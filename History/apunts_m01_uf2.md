# Apunts de la UF2
## comands


- veure el historial de comands del terminal:
  $ history
  ```
 1  fdisk /dev/sda
   2  su-
   3  dnf update
   4  exit
   5  dnf install @virtualitzation
   6  dnf install @virtualization
   7  systemctl stop firewalld.service
   8  systemctl start firewalld.service
   9  systemctl stop firewalld.service
  10  systemctl disable firewalld.service
  11  dnf -y install vim gvim mc
  12  dnf update
  13  setenforce0
  14  setenforce 0
  15  sed -i -e s,'SELINUX=enforcing','SELINUX=permissive', /etc/selinux/config
  16  dnf -y install gpm
  17  sytemctl enable gpm.service
  18  systemctl enable gpm.service
  19  systemctl start gpm.service
```
- veure les particions:
$ lsblk

```
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]
```
- veure l'informaciodels disk

"# lshw -short -c disk"
```
H/W path               Device     Class          Description
============================================================
/0/100/1f.2/0.0.0      /dev/sda   disk           500GB ST500DM002-1BD14
```
"#lshw -businfo -c disk"
```
# lshw -businfo -c disk
Bus info          Device     Class          Description
=======================================================
scsi@0:0.0.0      /dev/sda   disk           500GB ST500DM002-1BD14
```

"# dmidecode -t   bios"
system
aseboard
chassis
processor
memory
cache
connector
slot

sanitize
Remove potentially sensible information from output (IP addresses, serial numbers, etc.).

*dades=binari  velocitat=decimal*

MBps= mega*bytes* per segon
Mbps= mega*bits* per segon

Terabits (x/)1000(x/)1000(x/)1000(x/)1000
Gigabits (x/)1000(x/)1000(x/)1000
Megabits (x/)1000(x/)1000
Kilobits (x/)1000
Bits- (x/)8
bytes- (x/)1024
Kilobytes- 1024 1024
Megabytes- (x/)1024(x/)1024(x/)1024
Migabytes-(x/)1024(x/)1024(x/)1024(x/)1024
Terabytes-(x/)1024(x/)1024(x/)1024(x/)1024(x/)1024

